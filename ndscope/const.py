import os
import re
from dateutil.tz import tzutc, tzlocal


try:
    HOSTPORT = os.getenv('NDSSERVER').split(',')[0].split(':')
except AttributeError:
    HOSTPORT = [None]
HOST = HOSTPORT[0]
try:
    PORT = int(HOSTPORT[1])
except IndexError:
    PORT = 31200


# date/time formatting for GPS conversion
if os.getenv('DATETIME_TZ') == 'LOCAL':
    DATETIME_TZ = tzlocal()
else:
    DATETIME_TZ = tzutc()
# FIXME: why does '%c' without explicit TZ give very wrong values??
#DATETIME_FMT = '%c'
DATETIME_FMT = '%a %b %d %Y %H:%M:%S %Z'
DATETIME_FMT_OFFLINE = '%Y/%m/%d %H:%M:%S %Z'


TREND_THRESHOLD_S = 120
TREND_THRESHOLD_M = 3600


# percentage of full span to add as additional padding when fetching
# new data
DATA_SPAN_PADDING = 0.5


# number of lookback bytes available per channel
# 2**22:             4194304
# one week of 16 Hz: 4838400
# 60s of 16k Hz:     7864320
# 2**23:             8388608
DATA_LOOKBACK_LIMIT_BYTES = 2**22


CHANNEL_REGEXP = '^([a-zA-Z0-9-]+:)?[a-zA-Z0-9-_\.]+$'
CHANNEL_RE = re.compile(CHANNEL_REGEXP)
